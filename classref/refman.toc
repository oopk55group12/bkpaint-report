\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Ki\IeC {\'\ecircumflex }n tr\IeC {\'u}c Class}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Danh m\IeC {\d u}c c\IeC {\'a}c Class}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Danh m\IeC {\d u}c File}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Class}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Add\discretionary {-}{}{}Image Class Tham chi\IeC {\'\ecircumflex }u}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{9}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}Add\discretionary {-}{}{}Image}{9}{subsubsection.4.1.2.1}
\contentsline {subsection}{\numberline {4.1.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{9}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}load}{9}{subsubsection.4.1.3.1}
\contentsline {subsubsection}{\numberline {4.1.3.2}operate}{9}{subsubsection.4.1.3.2}
\contentsline {subsubsection}{\numberline {4.1.3.3}save}{10}{subsubsection.4.1.3.3}
\contentsline {subsubsection}{\numberline {4.1.3.4}type}{10}{subsubsection.4.1.3.4}
\contentsline {section}{\numberline {4.2}Add\discretionary {-}{}{}Shape Class Tham chi\IeC {\'\ecircumflex }u}{10}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{13}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{13}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}Add\discretionary {-}{}{}Shape}{13}{subsubsection.4.2.2.1}
\contentsline {subsection}{\numberline {4.2.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{13}{subsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.3.1}append}{13}{subsubsection.4.2.3.1}
\contentsline {subsubsection}{\numberline {4.2.3.2}load}{13}{subsubsection.4.2.3.2}
\contentsline {subsubsection}{\numberline {4.2.3.3}operate}{14}{subsubsection.4.2.3.3}
\contentsline {subsubsection}{\numberline {4.2.3.4}save}{14}{subsubsection.4.2.3.4}
\contentsline {subsubsection}{\numberline {4.2.3.5}type}{14}{subsubsection.4.2.3.5}
\contentsline {section}{\numberline {4.3}Canvas Class Tham chi\IeC {\'\ecircumflex }u}{15}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{18}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{18}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Canvas}{18}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}$\sim $\discretionary {-}{}{}Canvas}{18}{subsubsection.4.3.2.2}
\contentsline {subsection}{\numberline {4.3.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{18}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}repaint\discretionary {-}{}{}Rect}{18}{subsubsection.4.3.3.1}
\contentsline {subsubsection}{\numberline {4.3.3.2}set\discretionary {-}{}{}Brush}{19}{subsubsection.4.3.3.2}
\contentsline {subsubsection}{\numberline {4.3.3.3}set\discretionary {-}{}{}Erase\discretionary {-}{}{}Radius}{19}{subsubsection.4.3.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.4}set\discretionary {-}{}{}Pen}{19}{subsubsection.4.3.3.4}
\contentsline {subsubsection}{\numberline {4.3.3.5}set\discretionary {-}{}{}Zoom\discretionary {-}{}{}Factor}{19}{subsubsection.4.3.3.5}
\contentsline {section}{\numberline {4.4}Change\discretionary {-}{}{}Shape Class Tham chi\IeC {\'\ecircumflex }u}{20}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{22}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{23}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}Change\discretionary {-}{}{}Shape}{23}{subsubsection.4.4.2.1}
\contentsline {subsection}{\numberline {4.4.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{23}{subsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.3.1}get\discretionary {-}{}{}Add\discretionary {-}{}{}Shape}{23}{subsubsection.4.4.3.1}
\contentsline {subsubsection}{\numberline {4.4.3.2}load}{23}{subsubsection.4.4.3.2}
\contentsline {subsubsection}{\numberline {4.4.3.3}operate}{23}{subsubsection.4.4.3.3}
\contentsline {subsubsection}{\numberline {4.4.3.4}save}{24}{subsubsection.4.4.3.4}
\contentsline {subsubsection}{\numberline {4.4.3.5}set\discretionary {-}{}{}Changed\discretionary {-}{}{}Part}{24}{subsubsection.4.4.3.5}
\contentsline {subsubsection}{\numberline {4.4.3.6}set\discretionary {-}{}{}Last\discretionary {-}{}{}Top\discretionary {-}{}{}Left}{24}{subsubsection.4.4.3.6}
\contentsline {subsubsection}{\numberline {4.4.3.7}type}{25}{subsubsection.4.4.3.7}
\contentsline {subsection}{\numberline {4.4.4}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Member Data}{25}{subsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.4.1}changed\discretionary {-}{}{}Part}{25}{subsubsection.4.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.4.2}last\discretionary {-}{}{}Top\discretionary {-}{}{}Left}{25}{subsubsection.4.4.4.2}
\contentsline {section}{\numberline {4.5}Color\discretionary {-}{}{}Picker Class Tham chi\IeC {\'\ecircumflex }u}{25}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{29}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{29}{subsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.2.1}Color\discretionary {-}{}{}Picker}{29}{subsubsection.4.5.2.1}
\contentsline {subsection}{\numberline {4.5.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{30}{subsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.3.1}color\discretionary {-}{}{}At}{30}{subsubsection.4.5.3.1}
\contentsline {subsubsection}{\numberline {4.5.3.2}init\discretionary {-}{}{}Contex\discretionary {-}{}{}Menu}{30}{subsubsection.4.5.3.2}
\contentsline {subsubsection}{\numberline {4.5.3.3}init\discretionary {-}{}{}Palette}{30}{subsubsection.4.5.3.3}
\contentsline {subsubsection}{\numberline {4.5.3.4}mouse\discretionary {-}{}{}Move\discretionary {-}{}{}Event}{30}{subsubsection.4.5.3.4}
\contentsline {subsubsection}{\numberline {4.5.3.5}mouse\discretionary {-}{}{}Press\discretionary {-}{}{}Event}{31}{subsubsection.4.5.3.5}
\contentsline {subsubsection}{\numberline {4.5.3.6}palette\discretionary {-}{}{}Action\discretionary {-}{}{}\_\discretionary {-}{}{}triggered}{31}{subsubsection.4.5.3.6}
\contentsline {subsubsection}{\numberline {4.5.3.7}set\discretionary {-}{}{}Palette}{31}{subsubsection.4.5.3.7}
\contentsline {section}{\numberline {4.6}Erase Class Tham chi\IeC {\'\ecircumflex }u}{32}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{34}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{34}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}Erase}{34}{subsubsection.4.6.2.1}
\contentsline {subsection}{\numberline {4.6.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{34}{subsection.4.6.3}
\contentsline {subsubsection}{\numberline {4.6.3.1}load}{34}{subsubsection.4.6.3.1}
\contentsline {subsubsection}{\numberline {4.6.3.2}save}{34}{subsubsection.4.6.3.2}
\contentsline {subsubsection}{\numberline {4.6.3.3}type}{35}{subsubsection.4.6.3.3}
\contentsline {section}{\numberline {4.7}Fill Class Tham chi\IeC {\'\ecircumflex }u}{35}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{38}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{38}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}Fill}{38}{subsubsection.4.7.2.1}
\contentsline {subsection}{\numberline {4.7.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{38}{subsection.4.7.3}
\contentsline {subsubsection}{\numberline {4.7.3.1}load}{38}{subsubsection.4.7.3.1}
\contentsline {subsubsection}{\numberline {4.7.3.2}operate}{38}{subsubsection.4.7.3.2}
\contentsline {subsubsection}{\numberline {4.7.3.3}save}{39}{subsubsection.4.7.3.3}
\contentsline {subsubsection}{\numberline {4.7.3.4}type}{39}{subsubsection.4.7.3.4}
\contentsline {section}{\numberline {4.8}Main\discretionary {-}{}{}Window\discretionary {-}{}{}:\discretionary {-}{}{}:Large\discretionary {-}{}{}Icon\discretionary {-}{}{}Width\discretionary {-}{}{}Style Class Tham chi\IeC {\'\ecircumflex }u}{40}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{41}{subsection.4.8.1}
\contentsline {section}{\numberline {4.9}Main\discretionary {-}{}{}Window Class Tham chi\IeC {\'\ecircumflex }u}{42}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{47}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Member Data}{47}{subsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.2.1}icon\discretionary {-}{}{}Path}{47}{subsubsection.4.9.2.1}
\contentsline {section}{\numberline {4.10}Mouse\discretionary {-}{}{}Press Class Tham chi\IeC {\'\ecircumflex }u}{47}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{50}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{50}{subsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.2.1}load}{50}{subsubsection.4.10.2.1}
\contentsline {subsubsection}{\numberline {4.10.2.2}operate}{51}{subsubsection.4.10.2.2}
\contentsline {subsubsection}{\numberline {4.10.2.3}save}{51}{subsubsection.4.10.2.3}
\contentsline {subsubsection}{\numberline {4.10.2.4}set\discretionary {-}{}{}Changed\discretionary {-}{}{}Part}{51}{subsubsection.4.10.2.4}
\contentsline {subsubsection}{\numberline {4.10.2.5}set\discretionary {-}{}{}Last\discretionary {-}{}{}Top\discretionary {-}{}{}Left}{52}{subsubsection.4.10.2.5}
\contentsline {subsubsection}{\numberline {4.10.2.6}type}{52}{subsubsection.4.10.2.6}
\contentsline {subsection}{\numberline {4.10.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Member Data}{52}{subsection.4.10.3}
\contentsline {subsubsection}{\numberline {4.10.3.1}changed\discretionary {-}{}{}Part}{52}{subsubsection.4.10.3.1}
\contentsline {subsubsection}{\numberline {4.10.3.2}last\discretionary {-}{}{}Top\discretionary {-}{}{}Left}{52}{subsubsection.4.10.3.2}
\contentsline {section}{\numberline {4.11}New\discretionary {-}{}{}File\discretionary {-}{}{}Option Struct Tham chi\IeC {\'\ecircumflex }u}{53}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{53}{subsection.4.11.1}
\contentsline {section}{\numberline {4.12}New\discretionary {-}{}{}File\discretionary {-}{}{}Option\discretionary {-}{}{}Dialog Class Tham chi\IeC {\'\ecircumflex }u}{54}{section.4.12}
\contentsline {subsection}{\numberline {4.12.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{55}{subsection.4.12.1}
\contentsline {section}{\numberline {4.13}Op\discretionary {-}{}{}Pair Struct Tham chi\IeC {\'\ecircumflex }u}{56}{section.4.13}
\contentsline {subsection}{\numberline {4.13.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{56}{subsection.4.13.1}
\contentsline {section}{\numberline {4.14}Paint\discretionary {-}{}{}Operation Class Tham chi\IeC {\'\ecircumflex }u}{56}{section.4.14}
\contentsline {subsection}{\numberline {4.14.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{57}{subsection.4.14.1}
\contentsline {subsection}{\numberline {4.14.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{58}{subsection.4.14.2}
\contentsline {subsubsection}{\numberline {4.14.2.1}$\sim $\discretionary {-}{}{}Paint\discretionary {-}{}{}Operation}{58}{subsubsection.4.14.2.1}
\contentsline {subsection}{\numberline {4.14.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{58}{subsection.4.14.3}
\contentsline {subsubsection}{\numberline {4.14.3.1}load}{58}{subsubsection.4.14.3.1}
\contentsline {subsubsection}{\numberline {4.14.3.2}operate}{58}{subsubsection.4.14.3.2}
\contentsline {subsubsection}{\numberline {4.14.3.3}save}{59}{subsubsection.4.14.3.3}
\contentsline {subsubsection}{\numberline {4.14.3.4}type}{59}{subsubsection.4.14.3.4}
\contentsline {section}{\numberline {4.15}Color\discretionary {-}{}{}Picker\discretionary {-}{}{}:\discretionary {-}{}{}:Palette Struct Tham chi\IeC {\'\ecircumflex }u}{59}{section.4.15}
\contentsline {subsection}{\numberline {4.15.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{60}{subsection.4.15.1}
\contentsline {section}{\numberline {4.16}Color\discretionary {-}{}{}Picker\discretionary {-}{}{}:\discretionary {-}{}{}:Palette\discretionary {-}{}{}Color Struct Tham chi\IeC {\'\ecircumflex }u}{60}{section.4.16}
\contentsline {subsection}{\numberline {4.16.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{61}{subsection.4.16.1}
\contentsline {section}{\numberline {4.17}Player Class Tham chi\IeC {\'\ecircumflex }u}{61}{section.4.17}
\contentsline {subsection}{\numberline {4.17.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{65}{subsection.4.17.1}
\contentsline {subsection}{\numberline {4.17.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } Constructor v\IeC {\`a} Destructor}{65}{subsection.4.17.2}
\contentsline {subsubsection}{\numberline {4.17.2.1}Player}{65}{subsubsection.4.17.2.1}
\contentsline {subsubsection}{\numberline {4.17.2.2}Player}{65}{subsubsection.4.17.2.2}
\contentsline {subsubsection}{\numberline {4.17.2.3}$\sim $\discretionary {-}{}{}Player}{65}{subsubsection.4.17.2.3}
\contentsline {subsection}{\numberline {4.17.3}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{65}{subsection.4.17.3}
\contentsline {subsubsection}{\numberline {4.17.3.1}get\discretionary {-}{}{}Buffer}{65}{subsubsection.4.17.3.1}
\contentsline {subsubsection}{\numberline {4.17.3.2}set\discretionary {-}{}{}Interval\discretionary {-}{}{}By\discretionary {-}{}{}Speed}{65}{subsubsection.4.17.3.2}
\contentsline {section}{\numberline {4.18}Set\discretionary {-}{}{}Brush Class Tham chi\IeC {\'\ecircumflex }u}{66}{section.4.18}
\contentsline {subsection}{\numberline {4.18.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{68}{subsection.4.18.1}
\contentsline {subsection}{\numberline {4.18.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{68}{subsection.4.18.2}
\contentsline {subsubsection}{\numberline {4.18.2.1}load}{68}{subsubsection.4.18.2.1}
\contentsline {subsubsection}{\numberline {4.18.2.2}operate}{68}{subsubsection.4.18.2.2}
\contentsline {subsubsection}{\numberline {4.18.2.3}save}{68}{subsubsection.4.18.2.3}
\contentsline {subsubsection}{\numberline {4.18.2.4}type}{69}{subsubsection.4.18.2.4}
\contentsline {section}{\numberline {4.19}Set\discretionary {-}{}{}Pen Class Tham chi\IeC {\'\ecircumflex }u}{69}{section.4.19}
\contentsline {subsection}{\numberline {4.19.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{72}{subsection.4.19.1}
\contentsline {subsection}{\numberline {4.19.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{72}{subsection.4.19.2}
\contentsline {subsubsection}{\numberline {4.19.2.1}load}{72}{subsubsection.4.19.2.1}
\contentsline {subsubsection}{\numberline {4.19.2.2}operate}{72}{subsubsection.4.19.2.2}
\contentsline {subsubsection}{\numberline {4.19.2.3}save}{72}{subsubsection.4.19.2.3}
\contentsline {subsubsection}{\numberline {4.19.2.4}type}{73}{subsubsection.4.19.2.4}
\contentsline {section}{\numberline {4.20}Set\discretionary {-}{}{}Render\discretionary {-}{}{}Hint Class Tham chi\IeC {\'\ecircumflex }u}{73}{section.4.20}
\contentsline {subsection}{\numberline {4.20.1}M\IeC {\^o} t\IeC {\h a} chi ti\IeC {\'\ecircumflex }t}{76}{subsection.4.20.1}
\contentsline {subsection}{\numberline {4.20.2}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } h\IeC {\`a}m th\IeC {\`a}nh vi\IeC {\^e}n}{76}{subsection.4.20.2}
\contentsline {subsubsection}{\numberline {4.20.2.1}load}{76}{subsubsection.4.20.2.1}
\contentsline {subsubsection}{\numberline {4.20.2.2}operate}{76}{subsubsection.4.20.2.2}
\contentsline {subsubsection}{\numberline {4.20.2.3}save}{76}{subsubsection.4.20.2.3}
\contentsline {subsubsection}{\numberline {4.20.2.4}type}{77}{subsubsection.4.20.2.4}
\contentsline {chapter}{\numberline {5}Th\IeC {\^o}ng tin v\IeC {\`\ecircumflex } File}{79}{chapter.5}
\contentsline {section}{\numberline {5.1}src/player.hh File Tham chi\IeC {\'\ecircumflex }u}{79}{section.5.1}
\contentsline {section}{\numberline {5.2}player.\discretionary {-}{}{}hh}{80}{section.5.2}
\contentsline {part}{Ch\IeC {\h i} s\IeC {\'\ocircumflex }}{83}{section.5.2}
