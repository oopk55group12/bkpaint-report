all: report manual installmanual

diagrams: Diagrams/usecase.mp
	cd Diagrams; mpost usecase.mp

report: clean diagrams Thesis.tex
	TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape Thesis.tex;
	makeindex Thesis.idx
	bibtex Thesis.aux
	TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape Thesis.tex;
	TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape Thesis.tex;
	latex_count=5 ; \
	while egrep -s 'Rerun (LaTeX|to get cross-references right)' Thesis.log && [ $$latex_count -gt 0 ] ;\
	    do \
	      echo "Rerunning latex...." ;\
	      TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape Thesis.tex; \
	      latex_count=`expr $$latex_count - 1` ;\
	    done

test: test.tex
	TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape test.tex;

update:
	TEXINPUTS=Diagrams/:classref/:${TEXINPUTS} pdflatex -shell-escape Thesis.tex;

manual: manual.tex 
	pdflatex manual.tex
	pdflatex manual.tex

installmanual: installmanual.tex 
	pdflatex installmanual.tex
	pdflatex installmanual.tex

clean:
	rm -f *.ps *.dvi *.aux *.toc *.idx *.ind *.ilg *.log *.out *.brf *.blg *.bbl 
