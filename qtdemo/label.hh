#ifndef _LABEL_HH_
#define _LABEL_HH_

#include <QLabel>

class Label : public QLabel
{
    Q_OBJECT
public:
    Label (QString const &str, QWidget *parent = nullptr);
public slots:
    void hide();
protected:
    void mousePressEvent (QMouseEvent *event) override;
signals:
    void clicked ();
};

#endif // _LABEL_HH_
