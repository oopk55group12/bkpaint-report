#include <QApplication>
#include <QWidget>
#include "label.hh"

int main (int argc, char *argv[]) 
{
    QApplication app (argc, argv);
    
    Label label1 ("hide label 2");
    Label label2 ("label 2");
    label1.show();
    label2.show();

    QObject::connect (&label1, SIGNAL (clicked()), 
                      &label2, SLOT (hide()));

    return app.exec();
}
