#include "label.hh"
#include <QMouseEvent>

Label::Label (QString const &str, QWidget *parent)
    : QLabel (str, parent)
{
}

void Label::mousePressEvent (QMouseEvent *)
{
    emit clicked();
}

void Label::hide()
{
    QLabel::hide ();
}
